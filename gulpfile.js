const clean = require('gulp-clean');
const copy = require('gulp-copy');
const gulp = require('gulp');
const sourceMaps = require('gulp-sourcemaps');
const stylus = require('gulp-stylus');
const ts = require('gulp-typescript');
const webpack = require('webpack-stream');
const webpackConfig = require('./client/app/webpack.config.js');
const autoPrefixer = require('gulp-autoprefixer');
const concat = require('gulp-concat');

function swallowError(err) {
	console.log(err.toString());
	this.emit('end');
}

// Generic assets

const ASSET_EXTENSIONS = ['gif', 'jpeg', 'jpg', 'mov', 'mp4', 'png', 'ttf', 'woff'];
const ASSET_SOURCE_PATHS = ASSET_EXTENSIONS.map(extension => 'client/**/*.' + extension);

gulp.task('clean:assets', () => {
	const ASSET_DEST_PATHS = ASSET_EXTENSIONS.map(extension => 'build/client/**/*.' + extension);
	return gulp
		.src(ASSET_DEST_PATHS)
		.pipe(clean());
});

gulp.task('copy:assets', ['clean:assets'], () => {
	return gulp
		.src(ASSET_SOURCE_PATHS)
		.pipe(gulp.dest('build/client'));
});

gulp.task('copy-watch:assets', ['copy:assets'], () => {
	return gulp.watch(ASSET_SOURCE_PATHS, ['copy:assets']);
});

// CSS

gulp.task('clean:css', () => {
	return gulp
		.src('build/client/**/*.css', { read: false })
		.pipe(clean());
});

gulp.task('build:css', ['clean:css'], () => {
	return gulp
		.src('client/**/*.styl')
		.pipe(sourceMaps.init())
		.pipe(stylus())
		.pipe(concat('app.css'))
		.pipe(autoPrefixer())
		.on('error', swallowError)
		.pipe(gulp.dest('build/client/styles'));
});

gulp.task('build-watch:css', ['build:css'], () => {
	return gulp.watch('client/**/*.styl', ['build:css']);
});

// Client-side TypeScript

gulp.task('clean:client-js', () => {
	return gulp
		.src(['build/client/**/*.js', 'build/client/**/*.js.map'], { read: false })
		.pipe(clean());
});

const clientProject = ts.createProject('client/app/tsconfig.json');

gulp.task('build:client-js', ['clean:client-js'], () => {
	const tsResult = clientProject
		.src()
		.pipe(webpack(webpackConfig))
		.on('error', swallowError)
		.pipe(gulp.dest('build/client/app'));
});

gulp.task('build-watch:client-js', ['build:client-js'], () => {
	return gulp.watch('client/**/*.ts', ['build:client-js']);
});

// Server-side TypeScript

gulp.task('clean:server-js', () => {
	return gulp
		.src(['build/server/**/*.js', 'build/server/**/*.js.map'], { read: false })
		.pipe(clean());
});

const serverProject = ts.createProject('server/tsconfig.json');

gulp.task('build:server-js', ['clean:server-js'], () => {
	const tsResult = serverProject
		.src()
		.pipe(sourceMaps.init())
		.pipe(serverProject());

	return tsResult
		.js
		.pipe(sourceMaps.write('.'))
		.pipe(gulp.dest('build/server'));
});

gulp.task('build-watch:server-js', ['build:server-js'], () => {
	return gulp.watch('server/**/*.ts', ['build:server-js']);
});

// Build general

gulp.task('clean', () => {
	return gulp
		.src('build')
		.pipe(clean());
});

gulp.task('build', ['copy:assets', 'build:css', 'build:client-js', 'build:server-js']);

gulp.task('build-watch', ['copy-watch:assets', 'build-watch:css', 'build-watch:client-js', 'build-watch:server-js']);

gulp.task('default', ['build']);
