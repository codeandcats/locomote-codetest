# Locomote Node.js Code Test

## About
Hi,

Thanks for allowing me to take part in this code test. I found it one of the more interesting code tests I've taken. Though I must admit not being able to use a front-end framework like Angular or React was like trying to code with two arms tied behind my back, hehe.

Here's a list of things I would have liked to have implemented had I more time:

- Make all but top-level component stateless and unidirectional
- Validation
	- Add server-side validation
	- Make client-side validation inline (not using an alert)
	- Focus problematic inputs
- Refactor search code into smaller functions
- Tests
- Linting
- Clean up the responsive breakpoints for mobile
- More usable UI

The remaining items were not implemented simply due to time. Happy to discuss any areas of concern over by either phone, email or in person. :)

Thank you for your consideration.

Ben

## Install
```
npm install
```

## Run
```
npm start
```
Or
```
./start.sh
```
