const dateStringRegex = /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}[-\+]\d+:\d+$/;

export function isDateString(value: string): boolean {
	return !!dateStringRegex.exec(value);
}

export function convertDateStringsToDates<T>(value: any): any {
	if (value && typeof value === 'object') {
		for (const key of Object.getOwnPropertyNames(value)) {
			value[key] = convertDateStringsToDates(value[key]);
		}
		return value;
	} else if (typeof value === 'string') {
		return isDateString(value) ? new Date(value) : value;
	} else {
		return value;
	}
}
