import { Airline } from './airline';
import { Airport } from './airport';
import { Plane } from './plane';

export interface FlightStop extends Airport {
	dateTime: Date;
}

export interface Flight {
	key: string;
	airline: Airline;
	flightNum: number;
	start: FlightStop;
	finish: FlightStop;
	plane: Plane;
	distance: number;
	durationMin: number;
	price: number;
}
