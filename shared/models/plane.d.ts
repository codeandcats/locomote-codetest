export interface Plane {
	code: string;
	shortName: string;
	fullName: string;
	manufacturer: string;
	model: string;
}
