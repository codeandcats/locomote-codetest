export type TripType = 'oneWay' | 'roundTrip';

export interface SearchParameters {
	tripType: TripType;
	departureCityName: string;
	arrivalCityName: string;
	departureDate: Date;
	returnDate?: Date;
}
