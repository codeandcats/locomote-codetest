import { Flight } from './flight';

export interface SearchResults {
	departureLeg: Flight[];
	returnLeg: Flight[];
}
