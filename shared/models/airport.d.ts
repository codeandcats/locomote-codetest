export interface Airport {
	airportCode: string;
	airportName: string;
	cityCode: string;
	cityName: string;
	countryCode: string;
	countryName: string;
	latitude: number;
	longitude: number;
	stateCode: string;
	timeZone: string;
}
