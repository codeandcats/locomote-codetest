import * as Koa from 'koa';
import * as staticFiles from 'koa-static'
import * as appRootPath from 'app-root-path';
import * as fs from 'fs-promise';
import * as path from 'path';

export function configure(app: Koa) {

	const ALLOWED_VENDOR_ASSET_FILE_NAMES: Array<string | RegExp> = [
		'bluebird/js/browser/bluebird.min.js',
		'bootstrap/dist/css/bootstrap.min.css',
		'bootstrap/dist/css/bootstrap.min.css.map',
		'bootstrap/dist/js/bootstrap.min.js',
		/^font-awesome\/(css|fonts)\/.+$/i,
		'jquery/dist/jquery.min.js',
		'jquery/dist/jquery.min.js.map',
		'linq/linq.min.js',
		'moment/min/moment.min.js',
		'pikaday/pikaday.js',
		'pikaday/css/pikaday.css',
	];

	function getVendorAssetFileName(fileName: string): string | undefined {
		fileName = fileName.replace(/^\/vendor\//i, '');

		const isAllowed = ALLOWED_VENDOR_ASSET_FILE_NAMES.reduce((result, pattern) => {
			if (result) {
				return result;
			}

			if (typeof pattern === 'string') {
				return fileName == pattern;
			} else {
				return !!pattern.exec(fileName);
			}
		}, false);

		if (!isAllowed) {
			return undefined;
		}

		fileName = path.join('node_modules', fileName);
		return fileName;
	}

	app.use(async function vendorFiles(context, next) {
		if (context.path.startsWith('/vendor/')) {
			const fileName = getVendorAssetFileName(context.path);

			if (fileName) {
				context.body = fs.createReadStream(fileName);
				return;
			}
		}

		await next();
	});
}
