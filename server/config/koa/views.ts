import * as Pug from 'koa-pug';
import * as appRootPath from 'app-root-path';
import * as Koa from 'koa';
import * as path from 'path';
import * as fs from 'fs-promise';
import * as httpStatus from 'http-status';

export function configure(app: Koa) {
	const VIEW_PATH = appRootPath.resolve('./client');

	const pug = new Pug({
		viewPath: VIEW_PATH,
		debug: false,
		pretty: false,
		compileDebug: false,
		locals: {},
		basedir: VIEW_PATH,
		app
	});

	app.use(async function viewRenderer(context, next) {
		const urlPath = context.path.trim().replace(/^\/$/, '') || 'app/index';
		const fileName = path.join(VIEW_PATH, urlPath + '.pug');

		await fs
			.stat(fileName)
			.then(stat => {
				if (stat.isFile()) {
					context.render(fileName, {}, undefined, true);
				}
			}, () => next());
	});
}
