import * as Koa from 'koa';
import * as Router from 'koa-router';
import * as request from 'request-promise';

export function configure(app: Koa) {
	const api = new Router({
		prefix: '/api'
	});

	const LOCOMOTE_API_PATH = 'http://node.locomote.com/code-task';

	api.get('/airlines', async context => {
		const response = await request.get(LOCOMOTE_API_PATH + '/airlines');
		context.type = 'application/json';
		context.body = response;
	});

	api.get('/airports/:search', async context => {
		const response = await request.get(LOCOMOTE_API_PATH + '/airports?q=' + context.params.search);
		context.type = 'application/json';
		context.body = response;
	});

	api.get('/search/:airlineCode', async context => {
		const { airlineCode } = context.params;
		const { from, to } = context.query;
		const { date } =  context.query;
		const url = `${ LOCOMOTE_API_PATH }/flight_search/${ airlineCode }?date=${ date }&from=${ from }&to=${ to }`;
		const response = await request.get(url);
		context.type = 'application/json';
		context.body = response;
	});

	app.use(api.routes());
}
