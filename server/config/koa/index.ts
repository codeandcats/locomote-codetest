import * as Koa from 'koa';
import * as logging from './logging';
import * as views from './views';
import * as api from './api';
import * as staticFiles from './staticFiles';
import * as vendorFiles from './vendorFiles';

export function configure(app: Koa) {
	logging.configure(app);
	views.configure(app);
	api.configure(app);
	staticFiles.configure(app);
	vendorFiles.configure(app);
}