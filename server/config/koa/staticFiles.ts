import * as Koa from 'koa';
import * as staticFiles from 'koa-static'
import * as appRootPath from 'app-root-path';

export function configure(app: Koa) {
	app.use(staticFiles(appRootPath.resolve('./build/client')));
}
