import * as Koa from 'koa';
import * as chalk from 'chalk';
import * as httpStatus from 'http-status';

export function configure(app: Koa) {
	async function requestLogger(context: Koa.Context, next: () => Promise<any>) {
		const startTime = Date.now();
		let error: Error | any | undefined;

		try {
			await next();
		}
		catch (err) {
			if (context.status === httpStatus.NOT_FOUND) {
				context.status = httpStatus.INTERNAL_SERVER_ERROR;
			}
			error = err;
		}
		finally {
			const duration = Date.now() - startTime;
			context.set('X-Response-Time', `${ duration }ms`);

			const method = chalk.gray(context.method);
			const path = context.path;
			const responseTime = chalk.gray(`${ duration } ms`);
			const statusColor = (context.status >= 200 && context.status < 300) ? chalk.green : chalk.red;
			const status = statusColor(`${ context.status } ${httpStatus[context.status] || ''}`.trim());

			console.log(`${ status } ${ method } ${ path } ${ responseTime }`);

			if (error) {
				console.log(chalk.red(error.message || error));
				if (error.stack) {
					console.log(chalk.gray(error.stack));
				}
			}

			console.log();
		}
	};

	app.use(requestLogger);
}
