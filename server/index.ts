import * as Koa from 'koa';
import { configure } from './config/koa';
import * as chalk from 'chalk';

const app = new Koa();

configure(app);

const PORT = 3000
app.listen(PORT, () => {
	console.log();
	console.log(chalk.green(`Listening on port ${ PORT }`));
	console.log();
});
