import { Component, ComponentElements } from '../common/component';
import { Flight } from '../../../shared/models/flight';
import * as moment from 'moment';
import { formatCurrency, formatTime } from '../common/formatting';

class FlightListElements extends ComponentElements {
	get table() { return this.find('table:first-of-type'); }
	get tbody() { return this.find('tbody'); }
	get unselectedRows() { return this.find('tr:not(.is-selected)'); }
}

function createCell(text: string, className?: string): JQuery<HTMLTableCellElement> {
	const cell = $('<td></td>');
	cell.text(text);
	if (className) {
		cell.addClass(className);
	}
	return cell as JQuery<HTMLTableCellElement>;
}

function createRowFromFlight(flight: Flight, isSelected: boolean, selectFlight: (flight: Flight | undefined) => void): JQuery<HTMLTableRowElement> {
	const row = $('<tr></tr>');

	if (isSelected) {
		row.addClass('is-selected');
	}
	row.append(createCell(flight.airline.name, 'airline-cell'));
	row.append(createCell(flight.airline.code + '-' + flight.flightNum, 'flight-number-cell'));
	row.append(createCell(formatTime(flight.start.dateTime), 'departure-time-cell text-center'));
	row.append(createCell(formatTime(flight.start.dateTime), 'arrival-time-cell text-center'));
	row.append(createCell(formatCurrency(flight.price), 'price-cell text-center'));
	const flightSelectorCell = createCell('', 'flight-selector-cell text-center');
	const button = $('<button type="button" class="btn btn-default select-flight"></button>');
	button.text(isSelected ? 'Change' : 'Select')
	button.on('click', () => {
		if (isSelected) {
			selectFlight(undefined);
		} else {
			selectFlight(flight);
		}
	});
	flightSelectorCell.append(button);
	row.append(flightSelectorCell);

	return row as JQuery<HTMLTableRowElement>;
}

interface FlightListOptions {
	selectFlight: (flight: Flight | undefined) => void;
}

export class FlightList extends Component<FlightListElements> {
	private flights: Flight[] = [];
	private selectedFlight: Flight | undefined;

	constructor(container: HTMLElement | JQuery<HTMLElement> | string, private options: FlightListOptions) {
		super(container, FlightListElements);
	}

	protected getTemplateUrl() {
		return 'app/flightList/flightList';
	}

	protected async ready() {
		this.updateRows();
	}

	public update(flights: Flight[], selectedFlight?: Flight | undefined) {
		this.flights = flights;
		this.selectedFlight = selectedFlight;
		this.updateRows();
	}

	private updateRows() {
		if (this.elements && this.elements.tbody) {
			this.elements.tbody.html('');

			const rows = this.flights.map(flight => {
				return createRowFromFlight(
					flight,
					flight === this.selectedFlight,
					flightToSelect => {
						this.selectedFlight = flightToSelect;
						this.options.selectFlight(flightToSelect);
						this.updateRows();
						if (flightToSelect) {
							this.hideUnselectedRows();
						}
					}
				);
			});

			this.elements.tbody.append(...rows);
		}
	}

	hideUnselectedRows() {
		this.elements.unselectedRows.fadeOut();
	}
}