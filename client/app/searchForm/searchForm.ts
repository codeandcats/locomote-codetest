import * as Pikaday from 'pikaday';
import * as Bluebird from 'bluebird';
import * as linq from 'linq';
import { Component, ComponentElements } from '../common/component';
import { FlightSearchService } from '../common/flightSearchService';
import { enableElements } from '../common/elements';
import { Airline } from '../../../shared/models/airline';
import { Airport } from '../../../shared/models/airport';
import { Flight } from '../../../shared/models/flight';
import * as moment from 'moment';
import { SearchParameters, TripType } from '../../../shared/models/searchParameters';
import { SearchResults } from '../../../shared/models/searchResults';

class SearchFormElements extends ComponentElements {
	get searchForm() { return this.find('form[name="search-form"]'); }
	get oneWay() { return this.find('#trip-type-one-way'); }
	get roundTrip() { return this.find('#trip-type-round-trip'); }
	get selectedTripType() { return this.find('input[name="trip-type"]:checked'); }
	get departureCityName() { return this.find('input[name="departure-city"]'); }
	get arrivalCityName() { return this.find('input[name="arrival-city"]'); }
	get $departureDate() { return this.find('input[name="departure-date"]'); }
	get returnDate() { return this.find('input[name="return-date"]'); }
	get validationMessage() { return this.find('.alert'); }
	get search() { return this.find('button[type="submit"]'); }
	get searchStatusText() { return this.find('.search-status-text'); }
	get progress() { return this.find('.search-progress'); }
	get progressBar() { return this.find('.progress-bar'); }
	get formInputs() { return this.find('input, button'); }
}

interface SearchFormOptions {
	searchComplete: (searchParameters: SearchParameters, searchResults: SearchResults) => void;
}

export class SearchForm extends Component<SearchFormElements> {
	constructor(container: HTMLElement | JQuery<HTMLElement> | string, private options: SearchFormOptions) {
		super(container, SearchFormElements);
	}

	private isSearching = false;
	private flightSearchService = new FlightSearchService();
	private departureDatePicker: Pikaday;
	private returnDatePicker: Pikaday;

	protected getTemplateUrl() {
		return 'app/searchForm/searchForm';
	}

	protected async ready() {
		this.departureDatePicker = new Pikaday({ field: this.elements.$departureDate[0] });
		this.returnDatePicker = new Pikaday({ field: this.elements.returnDate[0] });

		this.elements.returnDate.hide();
		this.elements.progress.hide();
		this.elements.validationMessage.hide();

		this.elements.oneWay.on('click', () => this.elements.returnDate.fadeOut());
		this.elements.roundTrip.on('click', () => this.elements.returnDate.fadeIn());

		this.elements.searchForm.on('submit', () => {
			this.search();
			return false;
		});

		this.elements.departureCityName.focus();
	}

	private searchAllAirlinesAndAirports(date: Date, airlines: Airline[], departureAirports: Airport[], arrivalAirports: Airport[]): Promise<Flight[]>[] {
		const result: Promise<Flight[]>[] = [];
		for (const airline of airlines) {
			for (const departureAirport of departureAirports) {
				for (const arrivalAirport of arrivalAirports) {
					const promise = this.flightSearchService.getFlights(
						airline.code,
						date,
						departureAirport.airportCode,
						arrivalAirport.airportCode
					);
					result.push(promise);
				}
			}
		}
		return result;
	}

	private updateProgressBar(current: number, max: number): void {
		const percent = Math.round((current / max) * 100);
		this.elements.progressBar.attr({
			'aria-valuenow': current,
			'aria-valuemax': max
		});
		this.elements.progressBar.css('width', percent + '%');
	}

	private async hideSearchForm(): Promise<void> {
		return new Bluebird<void>(resolve => {
			this.elements.searchForm
				.stop(true, true)
				.fadeOut({ queue: false })
				.slideUp(() => resolve());
		});
	}

	private async hideSearchProgress(): Promise<void> {
		return new Bluebird<void>(resolve => {
			this.elements.progress
				.stop(true, true)
				.fadeOut({ queue: false })
				.slideUp(() => {
					this.updateProgressBar(0, 1);
					resolve();
				});
		});
	}

	private async showSearchForm(): Promise<void> {
		return new Bluebird<void>(resolve => {
			this.elements.searchForm
				.stop(true, true)
				.fadeIn({ queue: false })
				.slideDown(() => resolve());
		});
	}

	private async showSearchProgress(): Promise<void> {
		return new Bluebird<void>(resolve => {
			this.elements.progress
				.stop(true, true)
				.fadeIn({ queue: false })
				.slideDown(() => resolve());
		});
	}

	private getSearchFields(): SearchParameters {
		const tripType = this.elements.selectedTripType.val() as TripType;
		const departureCityName = '' + (this.elements.departureCityName.val() || '');
		const arrivalCityName = '' + (this.elements.arrivalCityName.val() || '');
		const departureDate = this.departureDatePicker.getDate();
		const returnDate = this.returnDatePicker.getDate();

		return {
			tripType,
			departureCityName,
			departureDate,
			arrivalCityName,
			returnDate
		}
	}

	private getRandomStatusText() {
		const MESSAGES: string[] = [
			'Enabling auto-pilot...',
			'Loading inflight entertainment...',
			'Restocking tiny beverages...',
			'Performing loop to loop...',
			'Doing a barrel role...',
			'Removing snakes...'
		];

		function getRandomInt(maxExclusive: number): number {
			return Math.floor(Math.random() * maxExclusive);
		}

		return MESSAGES[getRandomInt(MESSAGES.length)];
	}

	private getValidationMessage(searchParameters: SearchParameters): string | undefined {
		if (!('' + searchParameters.departureCityName).trim()) {
			return 'From city is required';
		}

		if (!('' + searchParameters.arrivalCityName).trim()) {
			return 'To city is required';
		}

		if (!searchParameters.departureDate) {
			return 'Departure date is required';
		}

		if (searchParameters.departureDate.getTime() < moment().endOf('day').toDate().getTime()) {
			return 'Departure date must be in future';
		}

		if (searchParameters.tripType === 'roundTrip') {
			if (!searchParameters.returnDate) {
				return 'Return date is required';
			}

			if (searchParameters.returnDate.getTime() < searchParameters.departureDate.getTime()) {
				return 'Return date must be later than departure date';
			}
		}
	}

	setValidationMessage(message: string | undefined) {
		if (message) {
			this.elements.validationMessage.text(message);
			this.elements.validationMessage.slideDown();
		} else {
			this.elements.validationMessage.hide();
		}
	}

	private disableForm() {
		this.elements.formInputs.attr('disabled', 'disabled');
	}

	private enableForm() {
		this.elements.formInputs.removeAttr('disabled');
	}

	private async search(): Promise<void> {
		if (this.isSearching) {
			return;
		}

		const searchParameters = this.getSearchFields();

		const validationMessage = this.getValidationMessage(searchParameters);
		this.setValidationMessage(validationMessage);
		if (validationMessage) {
			return;
		}

		this.isSearching = true;
		this.disableForm();

		// TODO: Perform airport and airline calls in parallel
		const departureAirports = await this.flightSearchService.getAirports(searchParameters.departureCityName);
		const arrivalAirports = await this.flightSearchService.getAirports(searchParameters.arrivalCityName);

		if (!departureAirports.length) {
			this.setValidationMessage(`From city "${ searchParameters.departureCityName }" not found`);
			this.isSearching = false;
			this.enableForm();
			return;
		}

		if (!arrivalAirports.length) {
			this.setValidationMessage(`To city "${ searchParameters.arrivalCityName }" not found`);
			this.isSearching = false;
			this.enableForm();
			return;
		}

		this.elements.searchStatusText.text('Finding flights...');
		this.hideSearchForm().then(() => this.showSearchProgress());

		let searchStatusTextTimer = setInterval(() => {
			this.elements.searchStatusText.text(this.getRandomStatusText());
		}, 1750);

		const airlines = await this.flightSearchService.getAirlines();

		const departureLegPromises = this.searchAllAirlinesAndAirports(searchParameters.departureDate, airlines, departureAirports, arrivalAirports);
		const returnLegPromises =
			searchParameters.tripType === 'roundTrip' ?
			this.searchAllAirlinesAndAirports(searchParameters.returnDate as Date, airlines, arrivalAirports, departureAirports) :
			[];
		const allPromises = departureLegPromises.concat(returnLegPromises);

		const departureLegFlights: Flight[] = [];
		const returnLegFlights: Flight[] = [];

		let completedSearchCount = 0;
		let failedSearchCount = 0;

		const updateProgress = () => {
			this.updateProgressBar(completedSearchCount, allPromises.length);
			if (completedSearchCount + failedSearchCount === allPromises.length) {
				this.isSearching = false;

				clearInterval(searchStatusTextTimer);
				this.enableForm();
				setTimeout(() => {
					this.showSearchForm().then(() => this.hideSearchProgress());
				}, 1000);

				if (this.options.searchComplete) {
					const searchResults = {
						departureLeg: departureLegFlights,
						returnLeg: returnLegFlights
					};
					this.options.searchComplete(searchParameters, searchResults);
				}
			}
		};

		function monitorSearchPromises(promises: Promise<Flight[]>[], results: Flight[]) {
			for (const promise of promises) {
				promise.then(flights => {
					completedSearchCount++;
					for (const flight of flights) {
						results.push(flight);
					}
					updateProgress();
				}, err => {
					failedSearchCount++;
					updateProgress();
				});
			}
		}

		monitorSearchPromises(departureLegPromises, departureLegFlights);
		monitorSearchPromises(returnLegPromises, returnLegFlights);
	}
}
