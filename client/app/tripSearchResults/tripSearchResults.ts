import { Component, ComponentElements } from '../common/component';
import { SearchParameters, TripType } from '../../../shared/models/searchParameters';
import { SearchResults } from '../../../shared/models/searchResults';
import { FlightList } from '../flightList/flightList';
import * as Bluebird from 'bluebird';
import { slideAndFadeIn, slideAndFadeOut } from '../common/elements';
import { Flight } from '../../../shared/models/flight';
import { formatCurrency } from '../common/formatting';

class TripSearchResultsElements extends ComponentElements {
	get searchResults() { return this.find('#search-results'); }
	get searchAgain() { return this.find('#search-again'); }
	get departureLegPanel() { return this.find('.panel.departure-leg'); }
	get departureFlightListContainer() { return this.find('#departure-flight-list-container'); }
	get returnLegPanel() { return this.find('.panel.return-leg'); }
	get returnFlightListContainer() { return this.find('#return-flight-list-container'); }
	get price() { return this.find('.price'); }
}

interface TripSearchResultsOptions {
	backToSearch: Function;
}

export class TripSearchResults extends Component<TripSearchResultsElements> {
	constructor(container: HTMLElement | JQuery<HTMLElement> | string, private options: TripSearchResultsOptions) {
		super(container, TripSearchResultsElements);
	}

	private searchParameters: SearchParameters;
	private searchResults: SearchResults;

	private departureFlightList: FlightList;
	private returnFlightList: FlightList;

	private departureFlight: Flight | undefined;
	private returnFlight: Flight | undefined;

	protected getTemplateUrl(): string {
		return '/app/tripSearchResults/tripSearchResults';
	}

	protected async ready(): Promise<void> {
		this.elements.searchAgain.on('click', () => this.options.backToSearch());
		this.departureFlightList = new FlightList(this.elements.departureFlightListContainer, {
			selectFlight: flight => {
				this.departureFlight = flight;
				this.updatePricing();
			}
		});
		this.returnFlightList = new FlightList(this.elements.returnFlightListContainer, {
			selectFlight: flight => {
				this.returnFlight = flight;
				this.updatePricing();
			}
		});
		this.updateTables();
		this.updatePricing();
	}

	public update(searchParameters: SearchParameters, searchResults: SearchResults) {
		this.searchParameters = searchParameters;
		this.searchResults = searchResults;
		this.updateTables();
	}

	private updateTables() {
		if (this.departureFlightList) {
			this.departureFlightList.update((this.searchResults && this.searchResults.departureLeg) || []);
		}
		if (this.returnFlightList) {
			const returnFlights = (this.searchResults && this.searchResults.returnLeg) || [];
			this.returnFlightList.update(returnFlights);
			if (returnFlights.length) {
				this.elements.returnLegPanel.show();
			} else {
				this.elements.returnLegPanel.hide();
			}
		}
	}

	private updatePricing() {
		const price =
			((this.departureFlight && this.departureFlight.price) || 0) +
			((this.returnFlight && this.returnFlight.price) || 0);

		this.elements.price.text(formatCurrency(price));
	}
}
