import * as $ from 'jquery';
import * as Bluebird from 'bluebird';

export function enableElements(element: Array<HTMLElement | JQuery<HTMLElement>>, isEnabled: boolean) {
	const $element = $(element);
	if (isEnabled) {
		$element.removeAttr('disabled');
	} else {
		$element.attr('disabled', 'disabled');
	}
}

type AnimationDuration = number | 'fast' | 'slow';

export async function fadeOut(element: JQuery<HTMLElement>, duration?: AnimationDuration): Promise<void> {
	return new Bluebird<void>(resolve => {
		element
			.stop(true, true)
			.fadeOut({
				queue: false,
				complete: () => resolve(),
				duration
			});
	});
}

export async function fadeIn(element: JQuery<HTMLElement>, duration?: AnimationDuration): Promise<void> {
	return new Bluebird<void>(resolve => {
		element
			.stop(true, true)
			.fadeIn({
				queue: false,
				complete: () => resolve(),
				duration
			});
	});
}

export async function slideAndFadeOut(element: JQuery<HTMLElement>): Promise<void> {
	return new Bluebird<void>(resolve => {
		element
			.stop(true, true)
			.fadeOut({ queue: false })
			.slideUp(() => resolve());
	});
}

export async function slideAndFadeIn(element: JQuery<HTMLElement>): Promise<void> {
	return new Bluebird<void>(resolve => {
		element
			.stop(true, true)
			.fadeIn({ queue: false })
			.slideDown(() => resolve());
	});
}
