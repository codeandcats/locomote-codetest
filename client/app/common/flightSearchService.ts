import * as $ from 'jquery';
import * as moment from 'moment';
import { Airline } from '../../../shared/models/airline';
import { Airport } from '../../../shared/models/airport';
import { Flight } from '../../../shared/models/flight';
import { convertDateStringsToDates } from '../../../shared/utils/dates';

export class FlightSearchService {
	async getAirlines(): Promise<Airline[]> {
		const airlines = await $.ajax('/api/airlines');
		return airlines;
	}

	async getAirports(cityName: string): Promise<Airport[]> {
		const airports = await $.ajax(`/api/airports/${ cityName }`);
		return airports;
	}

	async getFlights(airlineCode: string, date: Date, fromAirportCode: string, toAirportCode: string): Promise<Flight[]> {
		const DATE_FORMAT = 'YYYY-MM-DD';
		const dateFormatted = moment(date).format(DATE_FORMAT);
		const url = `/api/search/${ airlineCode }?date=${ dateFormatted }&from=${ fromAirportCode }&to=${ toAirportCode }`;
		let flights = await $.ajax(url);
		flights = convertDateStringsToDates(flights);
		return flights;
	}
}
