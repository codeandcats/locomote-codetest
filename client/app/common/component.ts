import * as $ from 'jquery';

export abstract class ComponentElements {
	constructor(protected container: JQuery<HTMLElement>) {
	}

	find(selector: string): JQuery<HTMLElement> {
		return this.container.find(selector);
	}
}

export interface ComponentElementsType<TElements extends ComponentElements> {
	new (container: JQuery<HTMLElement>): TElements;
}

export abstract class Component<TElements extends ComponentElements> {
	protected container: JQuery<HTMLElement>;
	protected elements: TElements;

	constructor(container: HTMLElement | JQuery<HTMLElement> | string, ElementsClass: ComponentElementsType<TElements>) {
		this.container = $(container);
		if (!this.container.length) {
			throw new Error('Component container not found');
		}

		$(async () => {
			await this.loadTemplate();
			this.elements = new ElementsClass(this.container);
			this.ready();
		});
	}

	protected getTemplateUrl(): string {
		return '';
	}

	protected async getTemplate(): Promise<string> {
		const url = this.getTemplateUrl();
		if (url) {
			const result = await $.ajax(url);
			return result;
		}
		return '';
	}

	protected async loadTemplate(): Promise<void> {
		const template = await this.getTemplate();
		this.container.html(template);
	}

	protected async ready(): Promise<void> {
	}
}
