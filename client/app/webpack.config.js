const webpack = require('webpack');
const path = require('path');
const appRootPath = require('app-root-path');

module.exports = {
	entry: {
		app: appRootPath.resolve('client/app/index')
	},
	output: {
		path: appRootPath.resolve('build/client/app'),
		filename: '[name].js',
		sourceMapFilename: '[name].js.map'
	},
	devtool: 'source-map',
	resolve: {
		modules: [
			"node_modules"
		],
		extensions: ['.ts']
	},
	module: {
		rules: [{
			test: /\.ts$/,
			loader: 'ts-loader',
			options: {
				configFile: './tsconfig.json'
			}
		}]
	},
	externals: {
		'jquery': 'jQuery',
		'linq': 'Enumerable',
		'moment': 'moment',
		'pikaday': 'Pikaday'
	},
	stats: {
		assets: false,
		colors: true,
		errors: true,
		errorDetails: true,
		hash: false
	}
};
