import * as $ from 'jquery';
import { Component, ComponentElements } from '../common/component';
import { SearchForm } from '../searchForm/searchForm';
import { TripSearchResults } from '../tripSearchResults/tripSearchResults';
import { fadeOut, fadeIn } from '../common/elements';

class MainPageElements extends ComponentElements {
	get searchFormContainer() { return this.find('#search-form-container'); }
	get searchResultsContainer() { return this.find('#search-results-container'); }
}

export class MainPage extends Component<MainPageElements> {
	constructor(container: HTMLElement | JQuery<HTMLElement> | string) {
		super(container, MainPageElements);
	}

	protected getTemplateUrl(): string {
		return 'app/mainPage/mainPage';
	}

	protected async ready() {
		this.elements.searchFormContainer.hide();
		this.elements.searchResultsContainer.hide();

		const searchForm = new SearchForm(this.elements.searchFormContainer, {
			searchComplete: (searchParameters, searchResults) => {
				tripSearchResults.update(searchParameters, searchResults);
				fadeOut(this.elements.searchFormContainer, 'fast').then(() => fadeIn(this.elements.searchResultsContainer, 'fast'));
			}
		});

		const tripSearchResults = new TripSearchResults(this.elements.searchResultsContainer, {
			backToSearch: () => {
				fadeOut(this.elements.searchResultsContainer, 'fast').then(() => fadeIn(this.elements.searchFormContainer, 'fast'));
			}
		});

		setTimeout(() => {
			fadeIn(this.elements.searchFormContainer, 'slow');
		}, 500);
	}
}
